package ppa.serversocket;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerSocketConnexionMultiThread implements Runnable {
	private ServerSocket serverSocket;
	private Socket socket;
	
	public Thread authentThread;
	
	public ServerSocketConnexionMultiThread(ServerSocket serverSocket) {
		this.serverSocket	= serverSocket;
	}
	
	@Override
	public void run() {
		try {
			while ( true ) {
				socket	= serverSocket.accept();
				System.out.println("connexion accepted by server : "  + serverSocket.getLocalSocketAddress() + " : " + serverSocket.getLocalPort());
				/*
				authentThread = new Thread (new OauthResponseHandler(socket));
				authentThread.start();
				*/
			}
		}
		catch ( IOException e ) {
			System.err.println("server error !!\n" + e.getMessage());
		}
		
	}
}
